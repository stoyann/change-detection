import { Component, NgZone, OnInit } from '@angular/core';
import { Person } from './components/people-list-on-push/people-list-default.component';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { interval, map, Subject, switchMap, takeWhile } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  peopleList: Person[] = [];
  arrOfUsers: Person[] = [];
  showTimeFlag = false; // TODO set to true for observables example
  timer$$ = interval(1000)
    .pipe(
      map(() => new Date().toLocaleTimeString())
    );

  constructor (private fb: FormBuilder) {
  }

  ngOnInit (): void {}

  addUser (): void {
    const newUsers = this.arrOfUsers.slice();
    newUsers.push(
      {
        name: 'Georgi',
        city: 'Varna',
        age: 45,
        id: 111
      }
    );

    this.arrOfUsers = newUsers;
  }

  loadPeopleList (): void {
    this.arrOfUsers = [
      ...[
          {
            name: 'Ivan',
            age: 23,
            city: 'Dobritch',
            id: 1
          },
          {
            name: 'Nicola',
            age: 24,
            city: 'Razgrad',
            id: 2
          },
          {
            name: 'Grigor',
            age: 24,
            city: 'Varna',
            id: 3
          },
          {
            name: 'Kaloyan',
            age: 25,
            city: 'Obzor',
            id: 4
          },
          {
            name: 'George',
            age: 27,
            city: 'Burgas',
            id: 5
          },
      ]
    ];
  }

  sameRef (): void {
    this.arrOfUsers.push(
      {
        name: 'Same ref',
        age: this.arrOfUsers.length + 10,
        city: 'Angular',
        id: this.arrOfUsers.length + 1
      }
    );
  }

  newRef (): void {
    this.arrOfUsers = [...this.arrOfUsers, ...[{
      name: 'New ref',
      age: this.arrOfUsers.length + 10,
      city: 'Angular',
      id: this.arrOfUsers.length + 1
    }]
    ];
  }

  deleteUserInternally (): void {
    this.arrOfUsers.splice(0, 1);
  }

  deletePerson(person: Person): void {
    const i = this.arrOfUsers.indexOf(person);
    this.arrOfUsers.splice(i, 1);
  }

  logSomething (): void {
    console.log('root component change detection run');
  }
}
