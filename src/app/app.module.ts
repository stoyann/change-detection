import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PeopleListDefaultComponent } from './components/people-list-on-push/people-list-default.component';
import { PeopleListOnPushComponent } from './components/people-list-default/people-list-on-push.component';

@NgModule({
  declarations: [
    AppComponent,
    PeopleListDefaultComponent,
    PeopleListOnPushComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
