import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, ComponentRef,
  EventEmitter,
  Input,
  OnInit,
  Output, Renderer2
} from '@angular/core';
import { interval, Observable } from 'rxjs';

export interface Person  {
  name: string;
  age: number;
  city: string;
  id: number;
}

@Component({
  selector: 'app-people-list-on-push',
  templateUrl: './people-list-on-push.component.html',
  styleUrls: ['./people-list-on-push.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PeopleListOnPushComponent implements OnInit{
  title = 'OnPush';
  time!: string;

  @Input() listOfPeople: Person[] = [];
  @Input() timer$$!: Observable<string>;
  @Input() showTimer!: boolean;

  @Output() removePerson = new EventEmitter();

  constructor(
    private changeRef: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    if (this.showTimer) {
      this.timer$$.subscribe((dateTime) => {
        this.time = dateTime;
        // this.changeRef.detectChanges();
      })
    }
  }

  changeDetectionPrint(): void {
    console.warn('OnPush component');
  }

  checkForChanges(): void {
    this.changeRef.detectChanges();
  }
}
