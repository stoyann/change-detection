import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, ComponentRef,
  EventEmitter,
  Input,
  OnInit,
  Output, Renderer2
} from '@angular/core';
import { interval, Observable } from 'rxjs';

export interface Person  {
  name: string;
  age: number;
  city: string;
  id: number;
}

@Component({
  selector: 'app-people-list-default',
  templateUrl: './people-list-default.component.html',
  styleUrls: ['./people-list-default.component.scss'],
})
export class PeopleListDefaultComponent implements OnInit{
  title = 'default';
  time!: string;

  @Input() listOfPeople: Person[] = [];
  @Input() timer$$!: Observable<string>;
  @Input() showTimer!: boolean;

  @Output() removePerson = new EventEmitter();

  dummyData = 0;

  constructor(
    private changeRef: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    if (this.showTimer) {
      this.timer$$.subscribe((dateTime) => {
        this.time = dateTime;
      })
    }
  }

  changeDetectionPrint(): void {
    console.warn('Default component');
  }


  private receiveDataFromObservable(): void {
    interval(1000)
      .subscribe((num) => {
        this.dummyData = num;
        this.changeRef.detectChanges();
      });
  }
}
